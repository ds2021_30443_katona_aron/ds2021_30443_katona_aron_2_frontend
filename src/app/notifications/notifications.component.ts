import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RxStompService} from "@stomp/ng2-stompjs";
import {Subject, Subscription} from "rxjs";
import {NgbAlert} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit, OnDestroy {
  notificationMessage: String = '';
  subscription: Subscription | null = null;
  @ViewChild('selfClosingAlert', {static: false}) selfClosingAlert!: NgbAlert;
  private _message = new Subject<string>();

  constructor(private rxStompService: RxStompService) {
  }

  ngOnInit(): void {
    this.subscription = this.rxStompService.watch("/user/queue/notifications").subscribe(
      message => {
        if (message.body) {
          this.changeNotification(message.body)
        }
      }
    )

    this._message.subscribe(message => this.notificationMessage = message)
    // this._message.pipe(debounceTime(5000)).subscribe(() => {
    //   if (this.selfClosingAlert) {
    //     this.selfClosingAlert.close()
    //   }
    // })
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private changeNotification(text: string): void {
    this._message.next(text)
  }
}
