import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ErrorPageComponent} from "./error-page/error-page.component";
import {ClientsComponent} from "./clients/clients.component";
import {DevicesComponent} from "./devices/devices.component";
import {HomeComponent} from "./home/home.component";
import {SensorsComponent} from "./sensors/sensors.component";
import {LoginComponent} from "./auth/login/login.component";
import {RoleGuardService} from "./auth/role-guard.service";
import {UserRole} from "./auth/user.model";
import {ConsumptionComponent} from "./consumption/consumption.component";
import {DeviceConsumptionComponent} from "./consumption/device-consumption/device-consumption.component";

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'clients',
    component: ClientsComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_ADMIN, title: 'Manage clients'}
  },
  {
    path: 'devices',
    component: DevicesComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_ADMIN, title: 'Manage devices'}
  },
  {
    path: 'sensors',
    component: SensorsComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_ADMIN, title: 'Manage sensors'}
  },
  {
    path: 'consumption',
    component: ConsumptionComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_CLIENT, title: 'Observe energy consumption'}
  },
  {
    path: 'consumption/device/:deviceId',
    component: DeviceConsumptionComponent,
    canActivate: [RoleGuardService],
    data: {expectedRole: UserRole.ROLE_CLIENT, title: 'Observe energy consumption of device'}
  },
  {path: 'login', component: LoginComponent, data: {title: 'Login'}},
  {path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!', title: 'Not found'}},
  {path: '**', redirectTo: '/not-found'},
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
