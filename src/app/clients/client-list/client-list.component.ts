import {Component, Input, OnInit} from '@angular/core';
import {Client} from "@energy-platform/energy-platform-api";


@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  @Input() clients: Client[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

}
