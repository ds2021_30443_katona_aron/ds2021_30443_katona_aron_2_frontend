import {Component, Input, OnInit} from '@angular/core';
import {Client} from "@energy-platform/energy-platform-api";

@Component({
  selector: 'app-client-list-item',
  templateUrl: './client-list-item.component.html',
  styleUrls: ['./client-list-item.component.css']
})
export class ClientListItemComponent implements OnInit {
  @Input() client!: Client

  constructor() {
  }

  ngOnInit(): void {
  }

}
