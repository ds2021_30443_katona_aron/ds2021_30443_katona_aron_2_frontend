import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Client} from "@energy-platform/energy-platform-api";
import {faPen, faTrash} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-client-table',
  templateUrl: './client-table.component.html',
  styleUrls: ['./client-table.component.css']
})
export class ClientTableComponent implements OnInit, OnChanges {
  @Input() clients: Client[] = [];
  @Output() edit = new EventEmitter<Client>()
  @Output() delete = new EventEmitter<Client>()
  @Input() optionsDisabled = false

  faPen = faPen;
  faTrash = faTrash;

  page = 1
  pageSize = 4;
  rows: { nr: number, client: Client }[] = []
  collectionSize = 0

  constructor() {
    this.refreshTable();
  }

  refreshTable() {
    this.rows = this.clients
      .map((client, i) => ({nr: i + 1, client: client}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);

  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.clients) {
      this.collectionSize = changes.clients.currentValue.length
      this.refreshTable()
    }
  }

  onEdit(client: Client) {
    this.edit.emit(client);
  }

  onDelete(client: Client) {
    this.delete.emit(client);
  }
}
