import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbDateAdapter, NgbDateNativeAdapter, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {faCalendar} from '@fortawesome/free-solid-svg-icons';
import {CreateClientDto} from "@energy-platform/energy-platform-api/model/createClientDto.model";

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css'],
  providers: [{provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}]
})
export class ClientCreateComponent implements OnInit, OnChanges {
  @ViewChild('content') modal!: ElementRef;
  @Input() isOpen = false;
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter()
  @Output() createSubmit: EventEmitter<CreateClientDto> = new EventEmitter()
  @Input() loading = false;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter()
  @Input() error: string = "";

  faCalendar = faCalendar;
  form: FormGroup;

  constructor(private modalService: NgbModal,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      birthDate: ['', Validators.required],
      address: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8)
      ])],
    });
  }

  get f(): { [p: string]: AbstractControl } {
    return this.form.controls;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isOpen) {
      const isOpen = changes.isOpen.currentValue
      this.toggleModal(isOpen)
    }
  }

  submitPressed() {
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loadingChange.emit(true)
    this.createSubmit.emit(this.form.value)
  }

  private toggleModal(open: boolean) {
    if (open) {
      this.form.reset();
      this.modalService.open(this.modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.isOpenChange.emit(false);
      }, () => {
        this.isOpenChange.emit(false);
      });
    } else {
      this.isOpenChange.emit(false);
      this.modalService.dismissAll();
    }
  }
}
