import {Component, OnInit} from '@angular/core';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {Client, ClientsService, UpdateClientDto} from "@energy-platform/energy-platform-api";
import {CreateClientDto} from "@energy-platform/energy-platform-api/model/createClientDto.model";

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clients$!: Observable<Client[]>
  faPlus = faPlus;
  openEditClient: boolean = false;

  clientToEdit!: Client;
  loading = false;
  formError: string = "";
  openCreateClient = false;

  constructor(private clientService: ClientsService) {
  }

  ngOnInit(): void {
    this.fetchData()
  }

  onEdit(client: Client) {
    this.clientToEdit = client;
    this.openEditClient = true;
  }

  onEditSubmit(updateClientDto: UpdateClientDto) {
    this.clientService.updateClient(this.clientToEdit.id, updateClientDto).subscribe(
      () => {
        this.openEditClient = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onDelete(client: Client) {
    this.clientService.deleteClient(client.id)
      .subscribe(() => {
        this.fetchData()
      }, error => {
        console.log(error)
      })
  }

  onCreate() {
    this.openCreateClient = true;
  }

  onCreateSubmit(createClientDto: CreateClientDto) {
    this.clientService.createClient(createClientDto).subscribe(
      () => {
        this.openCreateClient = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  private fetchData() {
    this.clients$ = this.clientService.viewAllClients()
      .pipe(map(clients => clients.map(
        client => {
          client.birthDate = new Date(client.birthDate);
          return client
        })));
  }
}
