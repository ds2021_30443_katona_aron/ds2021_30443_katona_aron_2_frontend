import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {faUser} from '@fortawesome/free-solid-svg-icons';
import {environment} from "../../environments/environment";
import {AuthService} from "../auth/auth.service";
import {Location} from '@angular/common';
import {User, UserRole} from "../auth/user.model";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  UserRole = UserRole;
  appTitle = environment.appTitle;
  collapsed = true;
  faUser = faUser;
  isLoggedIn = false;
  user: User | null = null;

  constructor(private router: Router,
              private location: Location,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.currentUser.subscribe(user => {
      this.isLoggedIn = user != null;
      this.user = user;
    });
  }

  onLogout(): void {
    this.authService.logout();
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router
        .navigate([currentUrl])
        .catch(() => this.router.navigate(['/']));
    });
    // location.reload();
  }

  onLogin(): void {
    this.router.navigate(['login'], {queryParams: {redirectURL: this.router.url}});
  }

}
