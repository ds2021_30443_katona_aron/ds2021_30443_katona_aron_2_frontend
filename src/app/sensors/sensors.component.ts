import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {CreateSensorDto, Sensor, SensorsService, UpdateSensorDto} from "@energy-platform/energy-platform-api";

@Component({
  selector: 'app-sensors',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.css']
})
export class SensorsComponent implements OnInit {
  sensors$!: Observable<Sensor[]>
  faPlus = faPlus;
  openEditModal: boolean = false;

  sensorToEdit!: Sensor;
  sensorToEditDevice!: Sensor;
  loading = false;
  formError: string = "";
  openCreateModal = false;
  openOwnerModal = false;

  constructor(private sensorsService: SensorsService) {
  }

  ngOnInit(): void {
    this.fetchData()
  }

  onEdit(sensor: Sensor) {
    this.sensorToEdit = sensor;
    this.openEditModal = true;
  }

  onEditSubmit(updateSensorDto: UpdateSensorDto) {
    this.sensorsService.updateSensor(this.sensorToEdit.id, updateSensorDto).subscribe(
      () => {
        this.openEditModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onDelete(sensor: Sensor) {
    this.sensorsService.deleteSensor(sensor.id)
      .subscribe(() => {
        this.fetchData()
      }, error => {
        console.log(error)
      })
  }

  onCreate() {
    this.openCreateModal = true;
  }

  onCreateSubmit(createSensorDto: CreateSensorDto) {
    this.sensorsService.createSensor(createSensorDto).subscribe(
      () => {
        this.openCreateModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onDeviceRemove(sensor: Sensor) {
    this.sensorsService.removeDeviceOfSensor(sensor.id).subscribe(
      () => {
        this.openOwnerModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onDeviceReplace(data: { sensor: Sensor; deviceId: string }) {
    this.sensorsService.assignDeviceToSensor(data.deviceId, data.sensor.id).subscribe(
      () => {
        this.openOwnerModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onDevicePressed(sensor: Sensor) {
    this.sensorToEditDevice = sensor;
    this.openOwnerModal = true;
  }

  private fetchData() {
    this.sensors$ = this.sensorsService.viewAllSensors();
  }
}
