import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {faPen, faTrash} from '@fortawesome/free-solid-svg-icons';
import {Sensor} from "@energy-platform/energy-platform-api";

@Component({
  selector: 'app-sensor-table',
  templateUrl: './sensor-table.component.html',
  styleUrls: ['./sensor-table.component.css']
})
export class SensorTableComponent implements OnInit {
  @Input() sensors: Sensor[] = [];
  @Output() edit = new EventEmitter<Sensor>()
  @Output() delete = new EventEmitter<Sensor>()
  @Output() devicePressed = new EventEmitter<Sensor>()
  @Input() optionsDisabled = false

  faPen = faPen;
  faTrash = faTrash;

  page = 1
  pageSize = 4;
  rows: { nr: number, sensor: Sensor }[] = []
  collectionSize = 0

  constructor() {
    this.refreshTable();
  }

  refreshTable() {
    this.rows = this.sensors
      .map((sensor, i) => ({nr: i + 1, sensor: sensor}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);

  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.sensors) {
      this.collectionSize = changes.sensors.currentValue.length
      this.refreshTable()
    }
  }

  onEdit(sensor: Sensor) {
    this.edit.emit(sensor);
  }

  onDelete(sensor: Sensor) {
    this.delete.emit(sensor);
  }

  onDevicePressed(sensor: Sensor) {
    this.devicePressed.emit(sensor);
  }

}
