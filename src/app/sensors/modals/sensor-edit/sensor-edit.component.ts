import {Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Sensor, UpdateSensorDto} from "@energy-platform/energy-platform-api";

@Component({
  selector: 'app-sensor-edit',
  templateUrl: './sensor-edit.component.html',
  styleUrls: ['./sensor-edit.component.css']
})
export class SensorEditComponent implements OnInit {
  @ViewChild('content') modal!: ElementRef;
  @Input() isOpen = false;
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter()
  @Input() sensor!: Sensor
  @Output() editSubmit: EventEmitter<UpdateSensorDto> = new EventEmitter()
  @Input() loading = false;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter()
  @Input() error: string = "";

  form: FormGroup;

  constructor(private modalService: NgbModal,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      description: ['', Validators.required],
      maxValue: ['', Validators.required],
    });
  }

  get f(): { [p: string]: AbstractControl } {
    return this.form.controls;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isOpen) {
      const isOpen = changes.isOpen.currentValue
      this.toggleModal(isOpen)
    }

    if (changes.sensor) {
      const newSensor = changes.sensor.currentValue;
      if (newSensor) {
        const {id, ...dto} = newSensor;
        this.form.setValue(dto);
      } else {
        this.form.reset();
      }
    }
  }

  submitPressed() {
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loadingChange.emit(true)
    this.editSubmit.emit(this.form.value)
  }

  private toggleModal(open: boolean) {
    if (open) {
      this.modalService.open(this.modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.isOpenChange.emit(false);
      }, () => {
        this.isOpenChange.emit(false);
      });
    } else {
      this.isOpenChange.emit(false);
      this.modalService.dismissAll();
    }
  }

}
