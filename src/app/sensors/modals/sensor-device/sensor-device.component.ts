import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {DevicesService, Sensor, SensorsService} from "@energy-platform/energy-platform-api";
import {Subscription} from "rxjs";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Device} from "@energy-platform/energy-platform-api/model/device.model";

@Component({
  selector: 'app-sensor-device',
  templateUrl: './sensor-device.component.html',
  styleUrls: ['./sensor-device.component.css']
})
export class SensorDeviceComponent implements OnInit, OnChanges {
  @ViewChild('content') modal!: ElementRef;
  @Input() isOpen = false;
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter()
  @Input() sensor!: Sensor
  @Output() replace: EventEmitter<{ sensor: Sensor, deviceId: string }> = new EventEmitter()
  @Output() remove: EventEmitter<Sensor> = new EventEmitter()
  @Input() loading = false;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter()
  @Input() error: string = "";

  devices: Device[] = [];
  devicesSubscription!: Subscription;

  selectedDeviceId!: string

  constructor(private sensorsService: SensorsService,
              private deviceService: DevicesService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.fetchClients();
  }

  replacePressed() {
    if (!this.selectedDeviceId) {
      this.removePressed()
      return;
    }

    this.loadingChange.emit(true)
    this.replace.emit({sensor: this.sensor, deviceId: this.selectedDeviceId})
  }

  removePressed() {
    this.loadingChange.emit(true)
    this.remove.emit(this.sensor)
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isOpen) {
      const isOpen = changes.isOpen.currentValue
      this.toggleModal(isOpen)
    }

    if (changes.sensor) {
      const sensor = changes.sensor.currentValue as Sensor;
      if (sensor) {
        this.sensorsService.viewDeviceOfSensor(sensor.id).subscribe(
          device => {
            this.selectedDeviceId = device?.id;
          }, error1 => {
            console.log(error1)
          }
        )
      }
    }
  }

  ngOnDestroy(): void {
    if (this.devicesSubscription) {
      this.devicesSubscription.unsubscribe();
    }
  }

  private fetchClients() {
    this.devicesSubscription = this.deviceService.viewAllDevices().subscribe(
      devices => {
        this.devices = devices;
      }
    )
  }

  private toggleModal(open: boolean) {
    if (open) {
      this.modalService.open(this.modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.isOpenChange.emit(false);
      }, () => {
        this.isOpenChange.emit(false);
      });
    } else {
      this.isOpenChange.emit(false);
      this.modalService.dismissAll();
    }
  }
}
