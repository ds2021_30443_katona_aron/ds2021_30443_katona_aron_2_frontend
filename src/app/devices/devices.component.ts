import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {DevicesService} from "@energy-platform/energy-platform-api";
import {Device} from "@energy-platform/energy-platform-api/model/device.model";
import {UpdateDeviceDto} from "@energy-platform/energy-platform-api/model/updateDeviceDto.model";
import {CreateDeviceDto} from "@energy-platform/energy-platform-api/model/createDeviceDto.model";

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {
  devices$!: Observable<Device[]>
  faPlus = faPlus;
  openEditModal: boolean = false;

  deviceToEdit!: Device;
  deviceToEditOwner!: Device;
  loading = false;
  formError: string = "";
  openCreateModal = false;
  openOwnerModal = false;

  constructor(private devicesService: DevicesService) {
  }

  ngOnInit(): void {
    this.fetchData()
  }

  onEdit(device: Device) {
    this.deviceToEdit = device;
    this.openEditModal = true;
  }

  onEditSubmit(updateDeviceDto: UpdateDeviceDto) {
    this.devicesService.updateDevice(this.deviceToEdit.id, updateDeviceDto).subscribe(
      () => {
        this.openEditModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onDelete(device: Device) {
    this.devicesService.deleteDevice(device.id)
      .subscribe(() => {
        this.fetchData()
      }, error => {
        console.log(error)
      })
  }

  onCreate() {
    this.openCreateModal = true;
  }

  onCreateSubmit(createDeviceDto: CreateDeviceDto) {
    this.devicesService.createDevice(createDeviceDto).subscribe(
      () => {
        this.openCreateModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onOwnerRemove(device: Device) {
    this.devicesService.removeOwnerOfDevice(device.id).subscribe(
      () => {
        this.openOwnerModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onOwnerReplace(data: { device: Device; clientId: string }) {
    this.devicesService.assignOwnerToDevice(data.device.id, data.clientId).subscribe(
      () => {
        this.openOwnerModal = false;
        this.formError = "";
        this.loading = false;
        this.fetchData();
      },
      error => {
        this.formError = "Invalid input"
        this.loading = false;
      }
    )
  }

  onOwnerPressed(device: Device) {
    this.deviceToEditOwner = device;
    this.openOwnerModal = true;
  }

  private fetchData() {
    this.devices$ = this.devicesService.viewAllDevices();
  }
}
