import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Device} from "@energy-platform/energy-platform-api/model/device.model";
import {faPen, faTrash} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-devices-table',
  templateUrl: './devices-table.component.html',
  styleUrls: ['./devices-table.component.css']
})
export class DevicesTableComponent implements OnInit, OnChanges {

  @Input() devices: Device[] = [];
  @Output() edit = new EventEmitter<Device>()
  @Output() delete = new EventEmitter<Device>()
  @Output() ownerPressed = new EventEmitter<Device>()
  @Input() optionsDisabled = false

  faPen = faPen;
  faTrash = faTrash;

  page = 1
  pageSize = 4;
  rows: { nr: number, device: Device }[] = []
  collectionSize = 0

  constructor() {
    this.refreshTable();
  }

  refreshTable() {
    this.rows = this.devices
      .map((device, i) => ({nr: i + 1, device: device}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);

  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.devices) {
      this.collectionSize = changes.devices.currentValue.length
      this.refreshTable()
    }
  }

  onEdit(device: Device) {
    this.edit.emit(device);
  }

  onDelete(device: Device) {
    this.delete.emit(device);
  }

  onOwnerPressed(device: Device) {
    this.ownerPressed.emit(device);
  }
}
