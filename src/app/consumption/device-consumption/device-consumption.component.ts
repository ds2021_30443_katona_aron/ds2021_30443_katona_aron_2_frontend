import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {EnergyConsumption, EnergyConsumptionService} from "@energy-platform/energy-platform-api";
import {faCalendar} from '@fortawesome/free-solid-svg-icons';
import {NgbDateAdapter, NgbDateNativeAdapter} from "@ng-bootstrap/ng-bootstrap";
import {RxStompService} from "@stomp/ng2-stompjs";
import {take} from "rxjs/operators";
import {Subscription} from "rxjs";

// import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-device-consumption',
  templateUrl: './device-consumption.component.html',
  styleUrls: ['./device-consumption.component.css'],
  providers: [{provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}]
})
export class DeviceConsumptionComponent implements OnInit, OnDestroy {

  deviceId: string | null = null;
  day: Date = new Date()
  faCalendar = faCalendar;

  // public barChartData: ChartDataSets[] = [];
  // public barChartLegend = true;
  // public barChartType: ChartType = 'bar';
  // public barChartLabels: Label[];
  // public barChartOptions: ChartOptions = {
  //   responsive: true,
  //   // We use these empty structures as placeholders for dynamic theming.
  //   scales: {xAxes: [{}], yAxes: [{}]},
  //   plugins: {
  //     datalabels: {
  //       anchor: 'end',
  //       align: 'end',
  //     }
  //   }
  // };
  // public barChartPlugins = [];
  public consumptions: EnergyConsumption[] = [];

  private stompSubscription: Subscription | null = null;

  constructor(private route: ActivatedRoute,
              private energyConsumptionService: EnergyConsumptionService,
              private rxStompService: RxStompService) {
    // var labels = Array<string>();
    // for (let i = 0; i < 23; i++) {
    //   labels.push(i + "")
    // }
    // this.barChartLabels = labels
    // console.log(labels)
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params) => {
        this.deviceId = params.deviceId;

        if (!this.deviceId) {
          return
        }

        this.drawChart();

      });

    this.stompSubscription = this.rxStompService.watch("/user/queue/update-consumptions").subscribe(
      () => {
        this.updateData()
      }
    )
  }

  ngOnDestroy(): void {
    if (this.stompSubscription) {
      this.stompSubscription.unsubscribe()
    }
  }

  private drawChart() {
    if (!this.day) {
      return
    }

    this.updateData();
  }

  private updateData() {
    if (!this.deviceId)
      return;
    this.energyConsumptionService.viewEnergyConsumptionOfDevice(this.deviceId)
      .pipe(take(1))
      .subscribe(
        consumptions => {
          this.consumptions = consumptions.map((elem) => {
            elem.timestamp = new Date(elem.timestamp)
            return elem
          })
        }
      )
  }

}
