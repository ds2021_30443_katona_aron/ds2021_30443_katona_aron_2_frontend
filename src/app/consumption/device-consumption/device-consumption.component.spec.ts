import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DeviceConsumptionComponent} from './device-consumption.component';

describe('DeviceConsumptionComponent', () => {
  let component: DeviceConsumptionComponent;
  let fixture: ComponentFixture<DeviceConsumptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DeviceConsumptionComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceConsumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
