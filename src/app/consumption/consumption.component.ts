import {Component, OnInit} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {User} from "../auth/user.model";
import {Client, ClientsService, EnergyConsumptionService} from "@energy-platform/energy-platform-api";
import {Device} from "@energy-platform/energy-platform-api/model/device.model";


@Component({
  selector: 'app-consumption',
  templateUrl: './consumption.component.html',
  styleUrls: ['./consumption.component.css']
})
export class ConsumptionComponent implements OnInit {

  user: User | null = null;
  client: Client | null = null
  devices: Device[] = []
  totalConsumption: number = 0;

  constructor(private authService: AuthService,
              private clientService: ClientsService,
              private energyConsumptionService: EnergyConsumptionService) {
  }

  ngOnInit(): void {
    this.authService.currentUser.subscribe(user => {
      const id = user?.clientId;
      if (!id) {
        this.user = null;
        this.client = null;
        this.devices = [];
        this.totalConsumption = 0;
        return
      }
      this.clientService.viewSingleClient(id).subscribe(
        client => {
          this.energyConsumptionService.viewTotalEnergyConsumptionOfClient(client.id).subscribe(
            totalConsumption => {
              this.clientService.viewDevicesOfClient(client.id).subscribe(
                devices => {
                  this.totalConsumption = totalConsumption
                  this.user = user;
                  this.client = client;
                  this.devices = devices
                }
              )
            }
          )
        }
      )

    });
  }

}
