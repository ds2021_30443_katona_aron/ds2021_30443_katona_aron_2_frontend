import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {first} from 'rxjs/operators';
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  redirectURL = '/';
  error = '';

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    if (authService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
  }

  // convenience getter for easy access to form fields
  get f(): { [p: string]: AbstractControl } {
    return this.form.controls;
  }

  ngOnInit(): void {


    this.route.queryParams
      .subscribe((params) => {
        this.redirectURL = params.redirectURL || '/';
      });
  }

  onSubmit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    const val = this.form.value;

    if (val.username && val.password) {
      this.authService.login(val.username, val.password)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigateByUrl(this.redirectURL, {replaceUrl: true});
            this.loading = false;
          },
          error => {
            this.error = 'Invalid credentials';
            this.loading = false;
          }
        );
    }
  }

}
