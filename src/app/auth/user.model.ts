export enum UserRole {
  ROLE_CLIENT = 'ROLE_CLIENT',
  ROLE_ADMIN = 'ROLE_ADMIN'
}

export class User {
  username: string = "";
  role: UserRole = UserRole.ROLE_CLIENT;
  clientId: string | null = null;
}
