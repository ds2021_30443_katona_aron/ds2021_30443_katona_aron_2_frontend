import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TokenService} from "../token.service";
import {environment} from "../../../environments/environment";
import {JWT_OPTIONS, JwtModule, JwtModuleOptions} from '@auth0/angular-jwt';

export function jwtOptionsFactory(tokenService: TokenService) {
  return {
    tokenGetter: () => {
      return tokenService.getToken();
    },
    allowedDomains: environment.allowedDomains,
    skipWhenExpired: true,
  };
}

const jwtModuleOptions: JwtModuleOptions = {
  jwtOptionsProvider: {
    provide: JWT_OPTIONS,
    useFactory: jwtOptionsFactory,
    deps: [TokenService]
  }
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    JwtModule.forRoot(jwtModuleOptions)
  ]
})
export class AppJwtModule {
}
