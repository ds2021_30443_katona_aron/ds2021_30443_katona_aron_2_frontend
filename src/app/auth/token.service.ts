import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  static readonly TOKEN_KEY = 'token';

  constructor() {
  }

  public getToken(): string {
    return localStorage.getItem(TokenService.TOKEN_KEY) || "";
  }

  public setToken(token: string): void {
    localStorage.setItem(TokenService.TOKEN_KEY, token);
  }

  public clearToken(): void {
    localStorage.removeItem(TokenService.TOKEN_KEY);
  }
}
